const toCache = [
  "/",
  "/icons/48.png",
  "/icons/96.png",
  "/icons/144.png",
  "/icons/192.png",
  "/icons/256.png",
  "/icons/512.png",
  "/favicon.ico",
  "/index.html",
  "/manifest.json",
  "/scripts.js",
  "/styles.css",
  "/sw.js",
];

self.addEventListener("install", event => {
  console.log("SW installed!");
  event.waitUntil(
    caches
      .open("static")
      .then((cache) => {
        cache.addAll(toCache);
      })
  );
});

self.addEventListener("activate", () => {
  console.log("SW activated!");
});

self.addEventListener("fetch", event => {
  console.log("SW is fetching...");
  event.respondWith(
    caches
      .match(event.request)
      .then(res => res || fetch(event.request))
  );
});
